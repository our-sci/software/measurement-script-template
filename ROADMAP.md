# Step by Step Examples

Here is an ordered list of examples that we should implement to get a user started.

## Hello World

Simple calculation example to show how to store a value and export it as CSV.

## CSV

Fetch a csv from Google and use values for calculation

## Getting Survey answer

Pull answer from survey and make a recommendation.

## Working with Mocks

Mock answer for the dev environment.

## Pull Sample Data from existing Survey

Show how to pull exisiting Data from Oursci as mock.\$

## Serial Communication

Communicate over USB with Arduino or similar to show how to do I/O.
