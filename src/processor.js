/* eslint-disable linebreak-style */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-param-reassign */

import {
  sprintf
} from 'sprintf-js';
// import _ from 'lodash';

import {
  app
} from '@oursci/scripts';
import * as ui from '@oursci/measurements-ui';
import '@oursci/measurements-ui/dist/styles.min.css';

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  // Process your results from sensor.js here
  // ...


  if (result.log) {
    // Display each entry from the results log array with an info card
    result.log.forEach((log) => {
      ui.info(log.title, log.content);
    });
  }

  if (result.error) {
    // Display each entry from the results error array with an error card
    result.error.forEach((msg) => {
      ui.error('Error', msg);
    });
    app.save();
    return;
  }
  app.save();
})();