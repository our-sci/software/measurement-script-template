import { app, formparser, serial } from '@oursci/scripts';

export default serial; // expose this to android for calling onDataAvailable

// if in dev environment, take this survey result as example
const uuidSnippet = '28c68e37';
const form = 'post-planting-anon-test';

if (app.initSubmittedMock && uuidSnippet && form) {
  app.initSubmittedMock(form, uuidSnippet);
  console.log(`fetched survey result for uuid: ${uuidSnippet}`);
}

const result = {};
result.log = [];
result.error = [];

// export const survey = surveyHelper();
export function err(msg) {
  result.error.push(msg);
}

function appendResult(title, content) {
  result.log.push({
    title,
    content,
  });
}

function errorExit(error) {
  app.error();
  throw Error(error);
}

(async () => {
  try {
    // Use `app.getAnswer('field_id')` to retreive a response from the survey by field ID
    // Call `appendResult(title, content)` to add an entry to the results array
    // Call `app.progress(progressVal)` to set the script progress in the app, where progressVal is an integer from 0 to 100
    // `app.getMeta()` can be used to reference the survey's structure (see docs for more info)

    // For an example, let's generate some dummy results
    const indexArray = Array.from(Array(10).keys()).forEach(i =>
      appendResult(`Title ${i}`, `Content ${i}`),
    );

    app.result(result);
  } catch (error) {
    console.error(error);
    result.error = [error.message];
    app.result(result);
  }
})();
